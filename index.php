<!DOCTYPE html>
<html>
<head>
    <title>Simple Modal with Markdown Support</title>
</head>
<body>
    <a href="#" class="modal" data-load-div="#myContentID">Modal with ID</a>
	<div class="simple-modal-div" id="myContentID">
			**Lorem ipsum dolor sit amet, consectetur adipisicing elit**

			Enim, repellat sit iusto commodi eveniet quo distinctio est non minus consequuntur. Accusamus, esse voluptatum quisquam eos ad ipsa in facere perspiciatis.
	</div>

	<a href="#" class="modal" data-load-div=".myContentClass">Modal with Class</a>
	<div class="simple-modal-div myContentClass">
			**Lorem ipsum dolor sit amet, consectetur adipisicing elit**

			Enim, repellat sit iusto commodi eveniet quo distinctio est non minus consequuntur. Accusamus, esse voluptatum quisquam eos ad ipsa in facere perspiciatis.

			![alt text](http://cdn.tutsplus.com/net.tutsplus.com/uploads/2013/06/procedural-to-oop-php-preview.jpg "Title")

			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, nisi, sint, id reprehenderit alias quasi odit debitis aperiam officiis animi laborum non dolores mollitia expedita ipsam eos veniam nulla cumque!

			<button onCLick="alert('asd');">Button</button>
	</div>

	<a href="#" class="modal" data-content="<p>**Lorem ipsum dolor sit amet**</p>consectetur adipisicing elit. Impedit, expedita, distinctio, nihil, cumque nulla id dicta obcaecati dignissimos soluta accusamus ab voluptatibus ullam iure beatae unde libero esse quos eum.">Modal with Inline Content</a>


    <script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
    <script src="jquery.simplemodal.js"></script>

	<script>
		$(document).ready(function(){
			$('.modal').simpleModal({
				width: '80%',
				maxWidth: '500px'
			});
		});
	</script>
</body>
</html>